package app.crescentcash.src.utils

import android.app.Activity

class DexUtil {
    companion object {
        fun isDeXEnabled(activity: Activity): Boolean {
            val config = activity.resources.configuration
            try {
                val configClass = config.javaClass
                return configClass.getField("SEM_DESKTOP_MODE_ENABLED").getInt(configClass) == configClass.getField("semDesktopModeEnabled").getInt(config)
            } catch (e: NoSuchFieldException) {
            } catch (e: IllegalAccessException) {
            } catch (e: IllegalArgumentException) {
            }

            return false
        }
    }
}