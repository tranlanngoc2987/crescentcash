package app.crescentcash.src.async

import android.os.AsyncTask
import android.view.View
import app.crescentcash.src.MainActivity
import app.crescentcash.src.manager.NetManager
import app.crescentcash.src.manager.UIManager
import app.crescentcash.src.manager.WalletManager
import app.crescentcash.src.utils.PrefsUtil
import kotlinx.android.synthetic.main.activity_main_dex.*
import org.bitcoinj.core.Address
import org.bitcoinj.core.AddressFactory
import org.bitcoinj.wallet.DeterministicSeed
import org.bitcoinj.wallet.Wallet

class AsyncTaskVerifyWallet(val activity: MainActivity, val tempWallet: Wallet, val cashAcctName: String, val seed: DeterministicSeed, val seedStr: String) : AsyncTask<Void, Void, Boolean>() {
    lateinit var cashAcctEmoji: String

    override fun doInBackground(vararg params: Void?): Boolean? {
        try {
            val cashAcctAddress = org.bitcoinj.net.NetHelper().getCashAccountAddress(WalletManager.parameters, cashAcctName)
            cashAcctEmoji = NetManager.getCashAccountEmoji(cashAcctName)
            println(cashAcctAddress)
            var accountAddress: Address? = null

            if (Address.isValidCashAddr(WalletManager.parameters, cashAcctAddress)) {
                accountAddress = AddressFactory.create().getAddress(WalletManager.parameters, cashAcctAddress)
            } else if (Address.isValidLegacyAddress(WalletManager.parameters, cashAcctAddress)) {
                accountAddress = Address.fromBase58(WalletManager.parameters, cashAcctAddress)
            } else {
                activity.runOnUiThread { UIManager.showToastMessage(activity, "No address found!") }
                return false
            }

            if (accountAddress != null) {
                return tempWallet.isPubKeyHashMine(accountAddress.hash160)
            } else {
                return false
            }
        } catch (e: NullPointerException) {
            activity.runOnUiThread { UIManager.showToastMessage(activity, "Cash Account not found.") }
            return false
        }
    }

    override fun onPostExecute(result: Boolean?) {
        super.onPostExecute(result)

        val isAddressMine = result!!
        if (isAddressMine) {
            WalletManager.setupSLPWallet(activity, seedStr, true)
            WalletManager.setupWalletKit(activity, seed, cashAcctName, true)
            val pref = PrefsUtil.prefs.edit()
            pref.putString("cashAccount", cashAcctName)
            pref.putString("cashEmoji", cashAcctEmoji)
            pref.putBoolean("isNewUser", false)
            pref.apply()

            activity.runOnUiThread {
                activity.restore_wallet.visibility = View.GONE
                activity.detailsBar.visibility = View.VISIBLE
                activity.displayDownloadContent(true)
                activity.runOnUiThread { UIManager.showToastMessage(activity, "Verified!") }
            }
        } else {
            activity.runOnUiThread { UIManager.showToastMessage(activity, "Verification failed!") }
        }
    }
}