package app.crescentcash.src.activity

import android.content.*
import android.os.Bundle
import android.provider.ContactsContract
import android.text.TextUtils
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import app.crescentcash.src.R
import app.crescentcash.src.listener.RecipientTextListener
import app.crescentcash.src.manager.NetManager
import app.crescentcash.src.manager.UIManager
import app.crescentcash.src.manager.WalletManager
import app.crescentcash.src.ui.RecipientEditText
import app.crescentcash.src.uri.URIHelper
import app.crescentcash.src.utils.Constants
import app.crescentcash.src.utils.DexUtil
import app.crescentcash.src.utils.PrefsUtil
import com.ncorti.slidetoact.SlideToActView
import org.bitcoinj.utils.MonetaryFormat
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*
import kotlin.collections.ArrayList

class SendActivity : AppCompatActivity() {
    private lateinit var tvRecipientAddress_AM: RecipientEditText
    lateinit var amountText: TextView
    lateinit var btnSendSlider: SlideToActView
    lateinit var btnSend: Button
    lateinit var setMaxCoins: Button
    lateinit var sendTypeSpinner: Spinner
    private lateinit var donateBtn: TextView
    private lateinit var fiatBalTxtSend: TextView
    private lateinit var bchBalSend: TextView
    private lateinit var contactsBtn: ImageButton
    lateinit var opReturnText: EditText
    lateinit var opReturnBox: LinearLayout
    private lateinit var qrScan: ImageView
    private var receiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (Constants.ACTION_CLEAR_SEND == intent.action) {
                this@SendActivity.clearSend()
            }
        }
    }
    val recipient: String
        get() = tvRecipientAddress_AM.text.toString().trim { it <= ' ' }

    val amount: String
        get() = amountText.text.toString()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        UIManager.determineTheme(this)
        this.setContentView(R.layout.send)
        this.findViews()
        this.prepareViews()
        this.initListeners()
    }

    private fun findViews() {
        tvRecipientAddress_AM = this.findViewById(R.id.tvRecipientAddress_AM)
        amountText = this.findViewById(R.id.etAmount_AM)
        setMaxCoins = this.findViewById(R.id.setMaxCoins)
        sendTypeSpinner = this.findViewById(R.id.sendType)
        donateBtn = this.findViewById(R.id.donateBtn)
        fiatBalTxtSend = this.findViewById(R.id.fiatBalTxtSend)
        bchBalSend = this.findViewById(R.id.bchBalSend)
        contactsBtn = this.findViewById(R.id.contactsBtn)
        opReturnText = this.findViewById(R.id.opReturnText)
        opReturnBox = this.findViewById(R.id.opReturnBox)
        qrScan = this.findViewById(R.id.qrScan)

        if(DexUtil.isDeXEnabled(this)) {
            btnSend = this.findViewById(R.id.btnSend)
            btnSend.visibility = View.VISIBLE
        } else {
            btnSendSlider = this.findViewById(R.id.btnSendSlider)
            btnSendSlider.visibility = View.VISIBLE
        }
    }

    private fun prepareViews() {
        this.displayMyBalance(WalletManager.getBalance(WalletManager.wallet).toFriendlyString())

        if (UIManager.showFiat) {
            this.updateFiatBalance()
        } else {
            fiatBalTxtSend.text = ""
        }

        val items = arrayOf(WalletManager.displayUnits, UIManager.fiat)
        val adapter = ArrayAdapter(this, R.layout.spinner_item, items)
        sendTypeSpinner.adapter = adapter

        when {
            WalletManager.sendType == WalletManager.displayUnits -> sendTypeSpinner.setSelection(0)
            WalletManager.sendType == UIManager.fiat -> sendTypeSpinner.setSelection(1)
        }

        sendTypeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, v: View?, position: Int, id: Long) {

                when (position) {
                    0 -> WalletManager.sendType = WalletManager.displayUnits
                    1 -> WalletManager.sendType = UIManager.fiat
                }

                PrefsUtil.prefs.edit().putString("sendType", WalletManager.sendType).apply()
                println(WalletManager.sendType)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        opReturnBox.visibility = if (WalletManager.addOpReturn) {
            View.VISIBLE
        } else {
            View.GONE
        }

        if(UIManager.streetModeEnabled) {
            this.setMaxCoins.isEnabled = false
            this.setMaxCoins.backgroundTintList = ContextCompat.getColorStateList(this, R.color.gray)
        }
    }

    private fun initListeners() {
        if(DexUtil.isDeXEnabled(this)) {
            this.btnSend.setOnClickListener { WalletManager.send(this@SendActivity) }
        } else {
            val slideBCHListener: SlideToActView.OnSlideCompleteListener = object : SlideToActView.OnSlideCompleteListener {
                override fun onSlideComplete(view: SlideToActView) {
                    WalletManager.send(this@SendActivity)
                }
            }
            this.btnSendSlider.onSlideCompleteListener = slideBCHListener
        }
        val listener: RecipientTextListener = object : RecipientTextListener {
            override fun onUpdate() {
                val clipboard = this@SendActivity.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val clipboardText = clipboard.text.toString()

                processScanOrPaste(clipboardText)
            }
        }
        this.setMaxCoins.setOnClickListener { setMaxCoins() }
        this.contactsBtn.setOnClickListener { showContactSelectionScreen() }
        this.tvRecipientAddress_AM.addListener(listener)
        this.donateBtn.setOnClickListener { this.displayRecipientAddress(Constants.DONATION_ADDRESS) }
        this.qrScan.setOnClickListener { UIManager.clickScanQR(this, Constants.REQUEST_CODE_SCAN_PAY_TO) }
        val filter = IntentFilter()
        filter.addAction(Constants.ACTION_CLEAR_SEND)
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter)
    }

    private fun setMaxCoins() {
        WalletManager.sendType = WalletManager.displayUnits
        sendTypeSpinner.setSelection(0)
        PrefsUtil.prefs.edit().putString("sendType", WalletManager.sendType).apply()
        var balBch: Double = if (WalletManager.selectedUtxos.size == 0) {
            java.lang.Double.parseDouble(WalletManager.getBalance(WalletManager.wallet).toPlainString())
        } else {
            java.lang.Double.parseDouble(WalletManager.getMaxValueOfSelectedUtxos().toPlainString())
        }

        val unit = WalletManager.displayUnits
        val coins = when (unit) {
            MonetaryFormat.CODE_BTC -> {
                UIManager.formatBalanceNoUnit(balBch, "#.########")
            }
            MonetaryFormat.CODE_MBTC -> {
                balBch *= 1000
                UIManager.formatBalanceNoUnit(balBch, "#.#####")
            }
            MonetaryFormat.CODE_UBTC -> {
                balBch *= 1000000
                UIManager.formatBalanceNoUnit(balBch, "#.##")
            }
            "sats" -> {
                balBch *= 100000000
                UIManager.formatBalanceNoUnit(balBch, "#")
            }
            else -> UIManager.formatBalanceNoUnit(balBch, "#.########")
        }

        println("Setting...")
        this.runOnUiThread { amountText.text = coins }
    }

    private fun showContactSelectionScreen() {
        val pickContact = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
        pickContact.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
        this.startActivityForResult(pickContact, Constants.REQUEST_CODE_GET_CONTACT)
    }

    fun displayRecipientAddress(recipientAddress: String?) {
        if (recipientAddress != null) {
            if (TextUtils.isEmpty(recipientAddress)) {
                tvRecipientAddress_AM.hint = this.resources.getString(R.string.receiver)
            } else {
                tvRecipientAddress_AM.setText(recipientAddress)
            }
        } else {
            tvRecipientAddress_AM.text = null
            tvRecipientAddress_AM.hint = this.resources.getString(R.string.receiver)
        }
    }

    fun clearSend() {
        this.setSendButtonsActive()
        this.displayRecipientAddress(null)
        this.clearAmount()
        this.opReturnText.text = null
        this.updateFiatBalance()
        WalletManager.selectedUtxos = ArrayList()
    }

    fun setSendButtonsActive() {
        if(DexUtil.isDeXEnabled(this)) {
            this.runOnUiThread { this.btnSend.isEnabled = true }
        } else {
            this.runOnUiThread {
                this.btnSendSlider.isEnabled = true
                this.btnSendSlider.resetSlider()
            }
        }
    }

    private fun clearAmount() {
        amountText.text = null
    }

    fun processScanOrPaste(text: String) {
        val uri = URIHelper(this, text, true)
        val address = uri.address

        if (address.startsWith("http")) {
            this.runOnUiThread {
                this.displayRecipientAddress(address)
                this.sendTypeSpinner.setSelection(0)
            }

            WalletManager.sendType = WalletManager.displayUnits
            PrefsUtil.prefs.edit().putString("sendType", WalletManager.sendType).apply()

            WalletManager.getBIP70Data(this, address)
        }
    }

    fun displayMyBalance(myBalance: String) {
        this.runOnUiThread {
            var balanceStr = myBalance
            balanceStr = balanceStr.replace(" BCH", "")
            var formatted = java.lang.Double.parseDouble(balanceStr)
            var balanceText = ""
            when (WalletManager.displayUnits) {
                MonetaryFormat.CODE_BTC -> {
                    balanceText = UIManager.formatBalance(formatted, "#,###.########")
                }
                MonetaryFormat.CODE_MBTC -> {
                    formatted *= 1000
                    balanceText = UIManager.formatBalance(formatted, "#,###.#####")
                }
                MonetaryFormat.CODE_UBTC -> {
                    formatted *= 1000000
                    balanceText = UIManager.formatBalance(formatted, "#,###.##")
                }
                "sats" -> {
                    formatted *= 100000000
                    balanceText = UIManager.formatBalance(formatted, "#,###")
                }
            }

            if(UIManager.streetModeEnabled) {
                bchBalSend.text = "########"
            } else {
                bchBalSend.text = balanceText
            }
        }
    }

    private fun updateFiatBalance() {
        object : Thread() {
            override fun run() {
                val coinBal = java.lang.Double.parseDouble(WalletManager.getBalance(WalletManager.wallet).toPlainString())
                val df = DecimalFormat("#,###.##", DecimalFormatSymbols(Locale.US))

                val fiatBalances = when (UIManager.fiat) {
                    "USD" -> {
                        val priceUsd = NetManager.price
                        val balUsd = coinBal * priceUsd

                        "$" + df.format(balUsd)
                    }
                    "EUR" -> {
                        val priceEur = NetManager.priceEur
                        val balEur = coinBal * priceEur

                        "€" + df.format(balEur)
                    }
                    "AUD" -> {
                        val priceAud = NetManager.priceAud
                        val balAud = coinBal * priceAud

                        "AUD$" + df.format(balAud)
                    }
                    else -> ""
                }

                runOnUiThread {
                    if(UIManager.streetModeEnabled) {
                        fiatBalTxtSend.text = "####"
                    } else {
                        fiatBalTxtSend.text = fiatBalances
                    }
                }
            }
        }.start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode != Constants.REQUEST_CODE_GET_CONTACT) {
            if (data != null) {
                val scanData = data.getStringExtra(Constants.QR_SCAN_RESULT)
                if (scanData != null) {
                    if (requestCode == Constants.REQUEST_CODE_SCAN_PAY_TO) {
                        this.processScanOrPaste(scanData)
                    }
                }
            }
        } else {
            if (data != null) {
                if (data.data != null) {
                    val contactData = data.data!!
                    val c = contentResolver.query(contactData, null, null, null, null)
                    if (c!!.moveToFirst()) {
                        val phoneIndex = c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER)
                        val num = c.getString(phoneIndex)
                        this.displayRecipientAddress(num)
                        c.close()
                    }
                } else {
                    UIManager.showToastMessage(this, "No contact selected.")
                }
            } else {
                UIManager.showToastMessage(this, "No contact selected.")
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        this.finish()
    }

    companion object
}
