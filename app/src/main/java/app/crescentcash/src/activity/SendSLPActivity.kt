package app.crescentcash.src.activity

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import app.crescentcash.src.R
import app.crescentcash.src.manager.UIManager
import app.crescentcash.src.manager.WalletManager
import app.crescentcash.src.uri.URIHelper
import app.crescentcash.src.utils.Constants
import app.crescentcash.src.utils.DexUtil
import com.ncorti.slidetoact.SlideToActView
import java.math.BigDecimal
import java.math.RoundingMode

class SendSLPActivity : AppCompatActivity() {
    private lateinit var setMaxSLP: Button
    lateinit var slpAmount: TextView
    lateinit var slpRecipientAddress: TextView
    private lateinit var slp_qrScan: ImageView
    lateinit var btnSendSLPSlider: SlideToActView
    lateinit var btnSendSLP: Button
    private lateinit var slpUnit: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        UIManager.determineTheme(this)
        this.setContentView(R.layout.send_slp)
        this.findViews()
        this.prepareViews(savedInstanceState)
        this.initListeners()
    }

    private fun findViews() {
        if(DexUtil.isDeXEnabled(this)) {
            btnSendSLP = this.findViewById(R.id.sendSLPBtn)
            btnSendSLP.visibility = View.VISIBLE
        } else {
            btnSendSLPSlider = this.findViewById(R.id.sendSLPBtn)
            btnSendSLPSlider.visibility = View.VISIBLE
        }
        setMaxSLP = this.findViewById(R.id.setMaxSLP)
        slpUnit = this.findViewById(R.id.slpUnit)
        slpAmount = this.findViewById(R.id.slpAmount)
        slpRecipientAddress = this.findViewById(R.id.slpRecipientAddress)
        slp_qrScan = this.findViewById(R.id.slp_qrScan)
    }

    private fun prepareViews(savedInstanceState: Bundle?) {
        this.slpUnit.text = if (savedInstanceState == null) {
            val extras = intent.extras
            if (extras == null) {
                ""
            } else {
                extras.getString(Constants.INTENT_TOKEN_TICKER_DATA)
            }
        } else {
            savedInstanceState.getSerializable(Constants.INTENT_TOKEN_TICKER_DATA).toString()
        }
    }

    private fun initListeners() {
        this.setMaxSLP.setOnClickListener {
            slpAmount.text = WalletManager.tokenListInfo[WalletManager.currentTokenPosition].amount.toPlainString()
        }

        if(DexUtil.isDeXEnabled(this)) {
            this.btnSendSLP.setOnClickListener {
                this@SendSLPActivity.send()
            }
        } else {
            val slideSLPListener: SlideToActView.OnSlideCompleteListener = object : SlideToActView.OnSlideCompleteListener {
                override fun onSlideComplete(view: SlideToActView) {
                    this@SendSLPActivity.send()
                }
            }
            this.btnSendSLPSlider.onSlideCompleteListener = slideSLPListener
        }

        this.slp_qrScan.setOnClickListener { UIManager.clickScanQR(this, Constants.REQUEST_CODE_SCAN_PAY_SLP_TO) }
    }

    private fun send() {
        if (!TextUtils.isEmpty(slpRecipientAddress.text) && !TextUtils.isEmpty(slpAmount.text)) {
            if (!slpRecipientAddress.text.toString().contains("#")) {
                val tokenId = WalletManager.currentTokenId
                val tokenPos = WalletManager.currentTokenPosition
                val tokenInfo = WalletManager.tokenListInfo[tokenPos]
                val amt = BigDecimal(java.lang.Double.parseDouble(slpAmount.text.toString())).setScale(tokenInfo.decimals!!, RoundingMode.HALF_UP)
                println(amt.toPlainString())
                WalletManager.sendToken(this@SendSLPActivity, tokenId, amt, slpRecipientAddress.text.toString())
            } else {
                this@SendSLPActivity.runOnUiThread { UIManager.showToastMessage(this@SendSLPActivity, "SLP CashAccts are not supported.") }
            }
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            val scanData = data.getStringExtra(Constants.QR_SCAN_RESULT)
            if (scanData != null) {
                if (requestCode == Constants.REQUEST_CODE_SCAN_PAY_SLP_TO) {
                    val uri = URIHelper(this, scanData, true)
                }
            }
        }
    }

    companion object
}
