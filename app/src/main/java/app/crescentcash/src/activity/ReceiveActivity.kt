package app.crescentcash.src.activity

import android.content.*
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Matrix
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import app.crescentcash.src.R
import app.crescentcash.src.hash.HashHelper
import app.crescentcash.src.manager.NetManager
import app.crescentcash.src.manager.UIManager
import app.crescentcash.src.manager.WalletManager
import app.crescentcash.src.utils.Constants
import app.crescentcash.src.utils.PrefsUtil
import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder



class ReceiveActivity : AppCompatActivity() {
    private lateinit var copyBtcAddr: ImageView
    private lateinit var btcAddress: TextView
    private lateinit var myCashHandle: TextView
    private lateinit var ivCopy_AM: ImageView
    private lateinit var myEmoji: TextView
    private lateinit var srlContent_AM: SwipeRefreshLayout
    private var cashAccount: String? = ""
    private var receiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (Constants.ACTION_UPDATE_CASH_ACCOUNT_LABEL == intent.action) {
                this@ReceiveActivity.cashAccount = PrefsUtil.prefs.getString("cashAccount", "")
                this@ReceiveActivity.displayCashAccount()
                this@ReceiveActivity.calculateEmoji()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        UIManager.determineTheme(this)
        this.setContentView(R.layout.receive)
        this.findViews()
        this.prepareViews(savedInstanceState)
        this.initListeners()
    }

    private fun prepareViews(savedInstanceState: Bundle?) {
        cashAccount = if (savedInstanceState == null) {
            val extras = intent.extras
            if (extras == null) {
                ""
            } else {
                extras.getString(Constants.INTENT_CASH_ACCOUNT_DATA)
            }
        } else {
            savedInstanceState.getString(Constants.INTENT_CASH_ACCOUNT_DATA)
        }

        this.calculateEmoji()
        this.displayCashAccount()
        this.displayEmoji()
    }

    private fun findViews() {
        copyBtcAddr = this.findViewById(R.id.copyBtcAddr)
        btcAddress = this.findViewById(R.id.btcAddress)
        myCashHandle = this.findViewById(R.id.myCashHandle)
        ivCopy_AM = this.findViewById(R.id.ivCopy_AM)
        myEmoji = this.findViewById(R.id.myEmoji)
        srlContent_AM = this.findViewById(R.id.srlContent_AM)
    }

    private fun initListeners() {
        this.srlContent_AM.setOnRefreshListener {
            this.calculateEmoji()
            this.displayCashAccount()
            this.displayEmoji()
        }
        this.ivCopy_AM.setOnClickListener {
            val cashAccount = PrefsUtil.prefs.getString("cashAccount", "")
            val clip = ClipData.newPlainText("My Cash Acct", cashAccount + "; " + myEmoji.text)
            val clipboard = this.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            clipboard.setPrimaryClip(clip)
            UIManager.showToastMessage(this, "Copied")
        }

        val copyListener = View.OnClickListener { copyAddr() }

        this.copyBtcAddr.setOnClickListener(copyListener)
        this.findViewById<ImageView>(R.id.btcQR).setOnClickListener(copyListener)
        val filter = IntentFilter()
        filter.addAction(Constants.ACTION_UPDATE_CASH_ACCOUNT_LABEL)
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter)
    }

    private fun copyAddr() {
        val clip: ClipData = ClipData.newPlainText("My BCH address", WalletManager.parameters.cashAddrPrefix + ":" + btcAddress.text.toString())
        val clipboard = this.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        clipboard.setPrimaryClip(clip)
        UIManager.showToastMessage(this, "Copied")
    }

    private fun displayEmoji() {
        val cashEmoji = PrefsUtil.prefs.getString("cashEmoji", "")
        myEmoji.text = cashEmoji

        if (srlContent_AM.isRefreshing) srlContent_AM.isRefreshing = false
    }


    fun displayCashAccount() {
        myCashHandle.text = cashAccount

        if (myCashHandle.text.toString().contains("#???")) {
            println("CASH ACCOUNT UNCONFIRMED")
            val cashAcctPlain = myCashHandle.text.toString().replace("#???", "")
            NetManager.initialAccountIdentityCheck(this, cashAcctPlain)
        }

        if (WalletManager.walletKit != null) {
            this.btcAddress.text = WalletManager.wallet.currentReceiveAddress().toString()
            btcAddress.text = this.btcAddress.text.toString().replace(WalletManager.parameters.cashAddrPrefix + ":", "")
            generateQR(this.btcAddress.text.toString(), R.id.btcQR, false)
        } else {
            this.btcAddress.text = "Loading..."
        }
    }

    private fun generateQR(textToConvert: String, viewID: Int, slp: Boolean) {
        try {
            val encoder = BarcodeEncoder()

            val qrCode = encoder.encodeBitmap(textToConvert, BarcodeFormat.QR_CODE, 1024, 1024)

            val coinLogo: Bitmap? = if (!slp)
                drawableToBitmap(this.resources.getDrawable(R.drawable.logo_bch))
            else
                drawableToBitmap(this.resources.getDrawable(R.drawable.logo_slp))

            val merge = overlayBitmapToCenter(qrCode, coinLogo!!)
            (this.findViewById<View>(viewID) as ImageView).setImageBitmap(merge)
        } catch (e: WriterException) {
            e.printStackTrace()
        }
    }

    /*
    I'm absolutely terrible with Bitmap and image generation shit. Always have been.

    Shout-out to StackOverflow for some of this.
     */
    private fun overlayBitmapToCenter(bitmap1: Bitmap, bitmap2: Bitmap): Bitmap {
        val bitmap1Width = bitmap1.width
        val bitmap1Height = bitmap1.height
        val bitmap2Width = bitmap2.width
        val bitmap2Height = bitmap2.height

        val marginLeft = (bitmap1Width * 0.5 - bitmap2Width * 0.5).toFloat()
        val marginTop = (bitmap1Height * 0.5 - bitmap2Height * 0.5).toFloat()

        val overlayBitmap = Bitmap.createBitmap(bitmap1Width, bitmap1Height, bitmap1.config)
        val canvas = Canvas(overlayBitmap)
        canvas.drawBitmap(bitmap1, Matrix(), null)
        canvas.drawBitmap(bitmap2, marginLeft, marginTop, null)
        return overlayBitmap
    }

    private fun drawableToBitmap(drawable: Drawable): Bitmap? {
        val bitmap: Bitmap? = if (drawable.intrinsicWidth <= 0 || drawable.intrinsicHeight <= 0) {
            Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888) // Single color bitmap will be created of 1x1 pixel
        } else {
            Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        }

        if (drawable is BitmapDrawable) {
            if (drawable.bitmap != null) {
                return drawable.bitmap
            }
        }

        val canvas = Canvas(bitmap!!)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }

    private fun calculateEmoji() {
        val hasEmoji = PrefsUtil.prefs.getString("cashEmoji", "")

        if (hasEmoji != null) {
            if (hasEmoji == "?" || hasEmoji == "") {
                object : Thread() {
                    override fun run() {
                        val cashAcctTx = PrefsUtil.prefs.getString("cashAcctTx", "null") as String
                        WalletManager.registeredTxHash = cashAcctTx

                        if (cashAcctTx != "null") {
                            try {
                                WalletManager.registeredBlockHash = NetManager.getTransactionData(WalletManager.registeredTxHash, "block_hash", "blockhash")

                                if (WalletManager.registeredBlock != "???") {
                                    try {
                                        val emoji = HashHelper().getCashAccountEmoji(WalletManager.registeredBlockHash, WalletManager.registeredTxHash)
                                        PrefsUtil.prefs.edit().putString("cashEmoji", emoji).apply()
                                        this@ReceiveActivity.myEmoji.text = emoji
                                        this@ReceiveActivity.displayEmoji()
                                    } catch (e: Exception) {
                                        this@ReceiveActivity.myEmoji.text = "?"
                                    }

                                } else {
                                    this@ReceiveActivity.myEmoji.text = "?"
                                }
                            } catch (e: NullPointerException) {
                                e.printStackTrace()
                                this@ReceiveActivity.myEmoji.text = "?"
                            }
                        } else {
                            try {
                                val cashAcctName = PrefsUtil.prefs.getString("cashAccount", "") as String
                                val cashAcctEmoji = NetManager.getCashAccountEmoji(cashAcctName)
                                PrefsUtil.prefs.edit().putString("cashEmoji", cashAcctEmoji).apply()
                                this@ReceiveActivity.myEmoji.text = cashAcctEmoji
                            } catch (e: Exception) {
                                e.printStackTrace()
                                runOnUiThread { UIManager.showToastMessage(this@ReceiveActivity, "Error getting emoji") }
                            }

                        }
                    }
                }.start()
            } else {
                myEmoji.text = hasEmoji
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(Constants.INTENT_CASH_ACCOUNT_DATA, myCashHandle.text.toString())
    }

    override fun onBackPressed() {
        super.onBackPressed()
        this.finish()
    }

    companion object
}
