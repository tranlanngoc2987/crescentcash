package app.crescentcash.src

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.UiThread
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import app.crescentcash.src.activity.*
import app.crescentcash.src.manager.NetManager
import app.crescentcash.src.manager.UIManager
import app.crescentcash.src.manager.WalletManager
import app.crescentcash.src.ui.NonScrollListView
import app.crescentcash.src.utils.Constants
import app.crescentcash.src.utils.DexUtil
import app.crescentcash.src.utils.PermissionHelper
import app.crescentcash.src.utils.PrefsUtil
import com.google.android.material.floatingactionbutton.FloatingActionButton
import org.bitcoinj.crypto.BIP38PrivateKey
import org.bitcoinj.utils.MonetaryFormat
import org.bitcoinj.wallet.Wallet
import java.io.File
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*


class MainActivity : AppCompatActivity() {
    private lateinit var createWalletBtn: Button
    private lateinit var restoreWalletBtn: Button
    lateinit var restore_wallet: FrameLayout
    lateinit var new_wallet: FrameLayout
    lateinit var newuser: FrameLayout
    private lateinit var balance: TextView
    private lateinit var openKeys: ImageButton
    private lateinit var registerUserBtn: Button
    lateinit var handle: EditText
    private lateinit var verifyUserBtn: Button
    lateinit var recoverySeed: EditText
    lateinit var handle2: EditText
    private lateinit var btnViewHistory: Button
    private lateinit var syncPct: TextView
    private lateinit var btnViewTokens: Button
    private lateinit var receiveFabHome: FloatingActionButton
    private lateinit var sendFabHome: FloatingActionButton
    private lateinit var fiatBalTxt: TextView
    lateinit var btnSweepPrivateKey: Button
    lateinit var btnScanPrivateKey: ImageView
    private lateinit var privKeyText: EditText
    private lateinit var bip38Layout: LinearLayout
    private lateinit var bip38PrivateKeyPassword: EditText
    lateinit var tabSend: Button
    lateinit var tabReceive: Button
    lateinit var tabSettings: Button
    private lateinit var txHistoryList: NonScrollListView
    private lateinit var no_tx_text: TextView
    private lateinit var srlHistory: SwipeRefreshLayout
    lateinit var detailsBar: LinearLayout

    private var receiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (Constants.ACTION_UPDATE_HOME_SCREEN_BALANCE == intent.action) {
                this@MainActivity.refresh()

                if(!UIManager.isDisplayingDownload) {
                    this@MainActivity.syncPct.text = ""
                }
            }
            if (Constants.ACTION_UPDATE_HOME_SCREEN_THEME == intent.action) {
                UIManager.determineTheme(this@MainActivity)
                if(DexUtil.isDeXEnabled(this@MainActivity)) {
                    this@MainActivity.setContentView(R.layout.activity_main_dex)
                } else {
                    this@MainActivity.setContentView(R.layout.activity_main)
                }
                this@MainActivity.findViews()
                this@MainActivity.prepareViews()
                this@MainActivity.initListeners()
                this@MainActivity.refresh()

                if(!UIManager.isDisplayingDownload) {
                    this@MainActivity.syncPct.text = ""
                }
            }
            if (Constants.ACTION_CLEAR_SWEEP_TEXT == intent.action) {
                this@MainActivity.privKeyText.text = null
                this@MainActivity.bip38PrivateKeyPassword.text = null
                this@MainActivity.bip38Layout.visibility = View.GONE
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataDirectory = applicationInfo.dataDir
        PrefsUtil.prefs = getSharedPreferences("app.crescentcash.src", Context.MODE_PRIVATE)
        PrefsUtil.loadPrefs()
        UIManager.determineTheme(this)

        if(DexUtil.isDeXEnabled(this)) {
            this.setContentView(R.layout.activity_main_dex)
        } else {
            this.setContentView(R.layout.activity_main)
        }

        this.findViews()
        this.prepareViews()
        this.initListeners()
        NetManager.establishProxy()
        WalletManager.setAPIKey()

        isNewUser = !File(this.applicationInfo.dataDir + "/users_wallet.wallet").exists()

        if (isNewUser) {
            this.newuser.visibility = View.VISIBLE

            if(DexUtil.isDeXEnabled(this))
                this.detailsBar.visibility = View.GONE
        } else {

            val hasSLPWallet = PrefsUtil.prefs.getBoolean("hasSLPWallet", false)

            if (hasSLPWallet)
                WalletManager.setupSLPWallet(this, null, true)
7
            if (savedInstanceState == null && WalletManager.walletKit == null) {
                WalletManager.setupWalletKit(this, null, "", false)
            } else {
                val intent = Intent(Constants.ACTION_UPDATE_HOME_SCREEN_BALANCE)
                LocalBroadcastManager.getInstance(this@MainActivity).sendBroadcast(intent)
            }

            this.displayDownloadContent(true)
            val cashAcct = PrefsUtil.prefs.getString("cashAccount", "")!!

            if (cashAcct != "" && cashAcct.contains("#???")) {
                val plainName = cashAcct.replace("#???", "")
                WalletManager.registeredBlock = "???"
                NetManager.initialAccountIdentityCheck(this, plainName)

                WalletManager.timer = object : CountDownTimer(150000, 20) {
                    override fun onTick(millisUntilFinished: Long) {

                    }

                    override fun onFinish() {
                        try {
                            NetManager.checkForAccountIdentity(this@MainActivity, plainName)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }
                }.start()
            }

        }

        if(!UIManager.isDisplayingDownload) {
            this@MainActivity.syncPct.text = ""
        }
    }

    private fun findViews() {
        createWalletBtn = this.findViewById(R.id.createWalletBtn)
        restoreWalletBtn = this.findViewById(R.id.restoreWalletBtn)
        restore_wallet = this.findViewById(R.id.restore_wallet)
        new_wallet = this.findViewById(R.id.new_wallet)
        newuser = this.findViewById(R.id.newuser)
        balance = this.findViewById(R.id.balance)
        registerUserBtn = this.findViewById(R.id.registerUserBtn)
        handle = this.findViewById(R.id.handle)
        verifyUserBtn = this.findViewById(R.id.verifyUserBtn)
        recoverySeed = this.findViewById(R.id.recoverySeed)
        handle2 = this.findViewById(R.id.handle2)
        fiatBalTxt = this.findViewById(R.id.fiatBalTxt)
        syncPct = this.findViewById(R.id.syncPct)
        btnViewTokens = this.findViewById(R.id.viewSLPBtn)

        if(DexUtil.isDeXEnabled(this)) {
            tabSend = this.findViewById(R.id.tabSend)
            tabReceive = this.findViewById(R.id.tabReceive)
            tabSettings = this.findViewById(R.id.tabSettings)
            txHistoryList = this.findViewById(R.id.txHistoryList)
            no_tx_text = this.findViewById(R.id.no_tx_text)
            srlHistory = this.findViewById(R.id.srlHistory)
            detailsBar = this.findViewById(R.id.detailsBar)
        } else {
            receiveFabHome = this.findViewById(R.id.receiveFabHome)
            sendFabHome = this.findViewById(R.id.sendFabHome)
            privKeyText = this.findViewById(R.id.privKeyMainText)
            bip38Layout = this.findViewById(R.id.bip38MainLayout)
            bip38PrivateKeyPassword = this.findViewById(R.id.bip38MainPrivKeyPass)
            btnSweepPrivateKey = this.findViewById(R.id.btnMainSweepPrivKey)
            btnScanPrivateKey = this.findViewById(R.id.btnMainScanPrivKey)
            openKeys = this.findViewById(R.id.openKeys)
            btnViewHistory = this.findViewById(R.id.viewBCHBtn)
        }

    }

    private fun prepareViews() {

    }

    private fun initListeners() {
        if(DexUtil.isDeXEnabled(this)) {
            this.tabReceive.setOnClickListener { this.displayReceive() }
            this.tabSend.setOnClickListener { startSendActivity() }
            this.tabSettings.setOnClickListener { UIManager.startActivity(this, SettingsActivity::class.java) }
            this.txHistoryList.setOnItemClickListener { parent, view, position, id ->
                val viewTxActivity = Intent(this, ViewTransactionActivity::class.java)
                viewTxActivity.putExtra(Constants.INTENT_TRANSACTION_POSITION_DATA, position)
                this.startActivity(viewTxActivity)
            }
            this.srlHistory.setOnRefreshListener { setArrayAdapter(WalletManager.wallet) }
        } else {
            this.openKeys.setOnClickListener { UIManager.startActivity(this, SettingsActivity::class.java) }
            this.receiveFabHome.setOnClickListener { this.displayReceive() }
            this.sendFabHome.setOnClickListener { startSendActivity() }
            this.btnScanPrivateKey.setOnClickListener { UIManager.clickScanQR(this, Constants.REQUEST_CODE_SWEEP_SCAN) }
            this.btnSweepPrivateKey.setOnClickListener {
                val privKey = privKeyText.text.toString()
                if (!TextUtils.isEmpty(privKey) && WalletManager.walletKit != null) {
                    if (!WalletManager.isEncryptedBIP38Key(privKey)) {
                        WalletManager.sweepWallet(this, privKey)
                        UIManager.showToastMessage(this, "Swept wallet!")
                    } else {
                        if(bip38Layout.visibility == View.GONE) {
                            bip38Layout.visibility = View.VISIBLE
                        } else {
                            val bip38Password = bip38PrivateKeyPassword.text.toString()
                            if(!TextUtils.isEmpty(bip38Password)) {
                                val encryptedKey = BIP38PrivateKey.fromBase58(WalletManager.parameters, privKey)
                                try {
                                    val ecKey = encryptedKey.decrypt(bip38Password)
                                    WalletManager.sweepWallet(this, ecKey.getPrivateKeyAsWiF(WalletManager.parameters))
                                    UIManager.showToastMessage(this, "Swept wallet!")
                                } catch (e: BIP38PrivateKey.BadPassphraseException) {
                                    UIManager.showToastMessage(this, "Incorrect password!")
                                }
                            } else {
                                UIManager.showToastMessage(this, "Please enter a password!")
                            }
                        }
                    }
                }
            }
            this.btnViewHistory.setOnClickListener {
                UIManager.startActivity(this, HistoryActivity::class.java)
            }
        }

        this.restoreWalletBtn.setOnClickListener { this.displayRestore() }
        this.createWalletBtn.setOnClickListener { this.displayNewWallet() }
        this.btnViewTokens.setOnClickListener {
            val tokensListActivity = Intent(this, TokensListActivity::class.java)
            tokensListActivity.putExtra(Constants.INTENT_BALANCE_DATA, WalletManager.findBitcoinInSLPList()?.amount?.toPlainString())
            this.startActivity(tokensListActivity)
        }

        val permissionHelper = PermissionHelper()
        permissionHelper.askForPermissions(this, this)

        val filter = IntentFilter()
        filter.addAction(Constants.ACTION_UPDATE_HOME_SCREEN_BALANCE)
        filter.addAction(Constants.ACTION_UPDATE_HOME_SCREEN_THEME)
        filter.addAction(Constants.ACTION_CLEAR_SWEEP_TEXT)
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter)
    }

    private fun startSendActivity() {
        UIManager.startActivity(this, SendActivity::class.java)
    }

    private fun displayNewWallet() {
        restore_wallet.visibility = View.GONE
        new_wallet.visibility = View.VISIBLE
        newuser.visibility = View.GONE

        if(DexUtil.isDeXEnabled(this))
            this.detailsBar.visibility = View.GONE

        registerUserBtn.setOnClickListener { NetManager.prepareWalletForRegistration(this) }
    }

    private fun displayRestore() {
        isNewUser = false
        restore_wallet.visibility = View.VISIBLE
        new_wallet.visibility = View.GONE
        newuser.visibility = View.GONE

        if(DexUtil.isDeXEnabled(this))
            this.detailsBar.visibility = View.GONE

        verifyUserBtn.setOnClickListener { NetManager.prepareWalletForVerification(this) }
    }

    @UiThread
    fun displayDownloadContent(status: Boolean) {
        WalletManager.downloading = status

        if (!status) {
            syncPct.text = ""
        }
    }

    private fun displayReceive() {
        val cashAccount = PrefsUtil.prefs.getString("cashAccount", "")
        val receiveActivity = Intent(this, ReceiveActivity::class.java)
        receiveActivity.putExtra(Constants.INTENT_CASH_ACCOUNT_DATA, cashAccount)
        this.startActivity(receiveActivity)
    }

    fun displayPercentage(percent: Int) {
        if (WalletManager.downloading)
            syncPct.text = "Syncing... $percent%"
    }

    fun displayMyBalance(myBalance: String) {
        this.runOnUiThread {
            var balanceStr = myBalance
            balanceStr = balanceStr.replace(" BCH", "")
            var formatted = java.lang.Double.parseDouble(balanceStr)
            var balanceText = ""
            when (WalletManager.displayUnits) {
                MonetaryFormat.CODE_BTC -> {
                    balanceText = UIManager.formatBalance(formatted, "#,###.########")
                }
                MonetaryFormat.CODE_MBTC -> {
                    formatted *= 1000
                    balanceText = UIManager.formatBalance(formatted, "#,###.#####")
                }
                MonetaryFormat.CODE_UBTC -> {
                    formatted *= 1000000
                    balanceText = UIManager.formatBalance(formatted, "#,###.##")
                }
                "sats" -> {
                    formatted *= 100000000
                    balanceText = UIManager.formatBalance(formatted, "#,###")
                }
            }

            if(UIManager.streetModeEnabled) {
                balance.text = "########"
            } else {
                balance.text = balanceText
            }
        }
    }

    private fun setArrayAdapter(wallet: Wallet) {
        setListViewShit(wallet)

        if (srlHistory.isRefreshing) srlHistory.isRefreshing = false
    }

    private fun setListViewShit(wallet: Wallet?) {
        if (wallet != null) {
            val txListFromWallet = wallet.getRecentTransactions(0, false)

            if (txListFromWallet != null && txListFromWallet.size != 0) {
                val txListFormatted = ArrayList<Map<String, String>>()
                WalletManager.txList = ArrayList()

                if (txListFromWallet.size > 0) {
                    no_tx_text.visibility = View.GONE
                    srlHistory.visibility = View.VISIBLE

                    for (x in 0 until txListFromWallet.size) {
                        val tx = txListFromWallet[x]
                        val confirmations = tx.confidence.depthInBlocks
                        val value = tx.getValue(wallet)
                        val datum = HashMap<String, String>()
                        var amountDbl = java.lang.Double.parseDouble(value.toPlainString().replace("-", ""))
                        val unit = WalletManager.displayUnits
                        var amountStr = ""

                        if (value.isPositive) {
                            when (unit) {
                                MonetaryFormat.CODE_BTC -> {
                                    amountStr = UIManager.formatBalance(amountDbl, "#,###.########")
                                }
                                MonetaryFormat.CODE_MBTC -> {
                                    amountDbl *= 1000
                                    amountStr = UIManager.formatBalance(amountDbl, "#,###.#####")
                                }
                                MonetaryFormat.CODE_UBTC -> {
                                    amountDbl *= 1000000
                                    amountStr = UIManager.formatBalance(amountDbl, "#,###.##")
                                }
                                "sats" -> {
                                    amountDbl *= 100000000
                                    amountStr = UIManager.formatBalance(amountDbl, "#,###")
                                }
                            }

                            datum["cashaccount"] = "false"
                            datum["cashshuffle"] = "false"
                            datum["slptx"] = "false"
                            val satoshiDice = WalletManager.isProtocol(tx, "02446365")
                            datum["satoshidice"] = satoshiDice.toString()
                            datum["action"] = "received"
                            val entry = String.format("%5s", amountStr)
                            datum["amount"] = entry
                            txListFormatted.add(datum)
                            WalletManager.txList.add(tx)
                        }

                        if (value.isNegative) {
                            when (unit) {
                                MonetaryFormat.CODE_BTC -> {
                                    amountStr = UIManager.formatBalance(amountDbl, "#,###.########")
                                }
                                MonetaryFormat.CODE_MBTC -> {
                                    amountDbl *= 1000
                                    amountStr = UIManager.formatBalance(amountDbl, "#,###.#####")
                                }
                                MonetaryFormat.CODE_UBTC -> {
                                    amountDbl *= 1000000
                                    amountStr = UIManager.formatBalance(amountDbl, "#,###.##")
                                }
                                "sats" -> {
                                    amountDbl *= 100000000
                                    amountStr = UIManager.formatBalance(amountDbl, "#,###")
                                }
                            }

                            datum["cashaccount"] = WalletManager.isProtocol(tx, "01010101").toString()
                            datum["cashshuffle"] = WalletManager.isCashShuffle(tx).toString()
                            datum["slptx"] = WalletManager.isProtocol(tx, "534c5000").toString()
                            datum["satoshidice"] = WalletManager.sentToSatoshiDice(tx).toString()
                            datum["action"] = "sent"
                            val entry = String.format("%5s", amountStr)
                            datum["amount"] = entry
                            txListFormatted.add(datum)
                            WalletManager.txList.add(tx)
                        }

                        when {
                            confirmations == 0 -> datum["confirmations"] = "0/unconfirmed"
                            confirmations < 6 -> datum["confirmations"] = "$confirmations/6 confirmations"
                            else -> datum["confirmations"] = "6+ confirmations"
                        }
                    }

                    val itemsAdapter = object : SimpleAdapter(this, txListFormatted, R.layout.transaction_list_cell, null, null) {
                        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                            // Get the Item from ListView
                            val view = LayoutInflater.from(this@MainActivity).inflate(R.layout.transaction_list_cell, null)
                            val action = txListFormatted[position]["action"]
                            val satoshiDice = txListFormatted[position]["satoshidice"]

                            if (action == "sent") {
                                val slptx = txListFormatted[position]["slptx"]
                                val cashShuffle = txListFormatted[position]["cashshuffle"]
                                val cashacct = txListFormatted[position]["cashaccount"]

                                if (slptx == "true") {
                                    val slpTxImg = view.findViewById<ImageView>(R.id.slptx)
                                    slpTxImg.visibility = View.VISIBLE
                                } else if (cashShuffle == "true") {
                                    val cashShuffleImg = view.findViewById<ImageView>(R.id.cashshuffle_icon)
                                    cashShuffleImg.visibility = View.VISIBLE
                                } else if (cashacct == "true") {
                                    val cashAcctImg = view.findViewById<ImageView>(R.id.cashacct_icon)
                                    cashAcctImg.visibility = View.VISIBLE
                                }
                            } else if (action == "received") {
                                view.findViewById<ImageView>(R.id.send).visibility = View.GONE
                                view.findViewById<ImageView>(R.id.receive).visibility = View.VISIBLE

                            }

                            if (satoshiDice == "true") {
                                val satoshiDiceImg = view.findViewById<ImageView>(R.id.satoshidice)
                                satoshiDiceImg.visibility = View.VISIBLE
                            }

                            // Initialize a TextView for ListView each Item
                            val text1 = view.findViewById(R.id.text1) as TextView
                            val text2 = view.findViewById(R.id.text2) as TextView

                            val amount = txListFormatted[position]["amount"]
                            val confirmations = txListFormatted[position]["confirmations"]
                            text1.text = amount.toString()
                            text2.text = confirmations.toString()
                            // Set the text color of TextView (ListView Item)

                            if (UIManager.nightModeEnabled) {
                                text1.setTextColor(Color.WHITE)
                                text2.setTextColor(Color.GRAY)
                            } else {
                                text1.setTextColor(Color.BLACK)
                            }

                            text2.ellipsize = TextUtils.TruncateAt.END
                            text2.maxLines = 1
                            text2.isSingleLine = true
                            // Generate ListView Item using TextView
                            return view
                        }
                    }
                    this.runOnUiThread { txHistoryList.adapter = itemsAdapter }
                } else {
                    srlHistory.visibility = View.GONE
                    no_tx_text.visibility = View.VISIBLE
                }
            }
        }
    }

    fun refresh() {
        if (UIManager.showFiat) {
            object : Thread() {
                override fun run() {
                    val coinBal = java.lang.Double.parseDouble(WalletManager.getBalance(WalletManager.wallet).toPlainString())
                    val df = DecimalFormat("#,###.##", DecimalFormatSymbols(Locale.US))

                    val fiatBalances = when (UIManager.fiat) {
                        "USD" -> {
                            val priceUsd = NetManager.price
                            val balUsd = coinBal * priceUsd

                            "$" + df.format(balUsd)
                        }
                        "EUR" -> {
                            val priceEur = NetManager.priceEur
                            val balEur = coinBal * priceEur

                            "€" + df.format(balEur)
                        }
                        "AUD" -> {
                            val priceAud = NetManager.priceAud
                            val balAud = coinBal * priceAud

                            "AUD$" + df.format(balAud)
                        }
                        else -> ""
                    }

                    this@MainActivity.runOnUiThread {
                        if(UIManager.streetModeEnabled) {
                            fiatBalTxt.text = "####"
                        } else {
                            fiatBalTxt.text = fiatBalances
                        }
                    }
                }
            }.start()
        } else {
            fiatBalTxt.text = ""
        }

        if(DexUtil.isDeXEnabled(this)) {
            this.runOnUiThread {
                setArrayAdapter(WalletManager.wallet)
            }
        }

        displayMyBalance(WalletManager.getBalance(WalletManager.wallet).toFriendlyString())
    }

    override fun onBackPressed() {
        when {
            this.new_wallet.visibility == View.VISIBLE -> {
                this.new_wallet.visibility = View.GONE
                this.newuser.visibility = View.VISIBLE
            }
            this.restore_wallet.visibility == View.VISIBLE -> {
                this.restore_wallet.visibility = View.GONE
                this.newuser.visibility = View.VISIBLE
            }
            else -> {
                val intent = Intent(Intent.ACTION_MAIN)
                intent.addCategory(Intent.CATEGORY_HOME)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.REQUEST_CODE_SWEEP_SCAN) {
            if (data != null) {
                val scanData = data.getStringExtra(Constants.QR_SCAN_RESULT)
                if (scanData != null) {
                    if(!WalletManager.isEncryptedBIP38Key(scanData)) {
                        this.privKeyText.setText(scanData)
                        this.bip38Layout.visibility = View.GONE
                    } else {
                        this.bip38Layout.visibility = View.VISIBLE
                        this.privKeyText.setText(scanData)
                    }
                }
            }
        }
    }

    companion object {
        var isNewUser = true
        lateinit var dataDirectory: String

        fun getDataDir(): String {
            return dataDirectory
        }
    }
}
